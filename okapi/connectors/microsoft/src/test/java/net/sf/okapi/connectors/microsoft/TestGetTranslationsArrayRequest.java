package net.sf.okapi.connectors.microsoft;

import static org.junit.Assert.assertEquals;

import java.io.InputStreamReader;
import java.io.StringReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import org.custommonkey.xmlunit.XMLAssert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import net.sf.okapi.common.FileLocation;

@RunWith(JUnit4.class)
public class TestGetTranslationsArrayRequest {
	private FileLocation root;

	@Before
	public void setup() {
		root = FileLocation.fromClass(getClass());
	}

	@Test
	@Deprecated // This is for API v2 which retires on 2019-4-30
	public void test() throws Exception {
		List<String> texts = new ArrayList<>();
		texts.add("string1");
		texts.add("string2");
		texts.add("string3");
		GetTranslationsArrayRequest data = new GetTranslationsArrayRequest(texts, "en", "fr", 1, "test-category");
		XMLAssert.assertXMLEqual(new InputStreamReader(root.in("/GetTranslationsArrayRequest.xml").asInputStream(),
				StandardCharsets.UTF_8), new StringReader(data.toXML()));
	}
	
	@Test
	public void testJsonRequestBody() throws Exception {
		List<String> texts = new ArrayList<>();
		texts.add("string1");
		texts.add("string2");
		texts.add("string3");
		GetTranslationsArrayRequest data = new GetTranslationsArrayRequest(texts, "en", "fr", 1, "test-category");
		assertEquals("[{\"text\":\"string1\"},{\"text\":\"string2\"},{\"text\":\"string3\"}]", data.toJSON());
	}
}
