/*
 * =============================================================================
 *   Copyright (C) 2010-2019 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.filters.openxml;

import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

interface PresentationNotesStyleDefinition extends StyleDefinition {
    String EMPTY = "";

    String id();

    class Empty implements PresentationNotesStyleDefinition {
        @Override
        public String id() {
            return EMPTY;
        }

        @Override
        public void readWith(XMLEventReader reader) throws XMLStreamException {
        }

        @Override
        public RunProperties runProperties() {
            return new RunProperties.Empty();
        }

        @Override
        public Markup toMarkup() {
            return new Markup.Empty();
        }
    }

    class ParagraphDefault implements PresentationNotesStyleDefinition {
        private static final String DEF_RPR = "defRPr";

        private final ConditionalParameters conditionalParameters;
        private final XMLEventFactory eventFactory;
        private final StartElement startElement;

        private RunProperties defaultRunProperties;

        ParagraphDefault(
            final ConditionalParameters conditionalParameters,
            final XMLEventFactory eventFactory,
            final StartElement startElement
        ) {
            this.conditionalParameters = conditionalParameters;
            this.eventFactory = eventFactory;
            this.startElement = startElement;
        }

        @Override
        public String id() {
            return EMPTY;
        }

        @Override
        public void readWith(XMLEventReader reader) throws XMLStreamException {
            while (reader.hasNext()) {
                final XMLEvent event = reader.nextEvent();
                if (event.isEndElement()) {
                    if (event.asEndElement().getName().equals(this.startElement.getName())) {
                        if (null == this.defaultRunProperties) {
                            this.defaultRunProperties = RunProperties.emptyRunProperties();
                        }
                        return;
                    }
                } else if (event.isStartElement()) {
                    if (DEF_RPR.equals(event.asStartElement().getName().getLocalPart())) {
                        this.defaultRunProperties = readDefaultRunProperties(event.asStartElement(), reader);
                    }
                }
            }
        }

        private RunProperties readDefaultRunProperties(final StartElement startElement, final XMLEventReader reader) throws XMLStreamException {
            final StartElementContext startElementContext = StartElementContextFactory.createStartElementContext(
                startElement,
                reader,
                this.eventFactory,
                this.conditionalParameters
            );
            return new RunPropertiesParser(
                startElementContext,
                new RunSkippableElements(startElementContext)
            )
            .parse();
        }

        @Override
        public RunProperties runProperties() {
            return this.defaultRunProperties;
        }

        @Override
        public Markup toMarkup() {
            throw new UnsupportedOperationException();
        }
    }

    class ParagraphLevel implements PresentationNotesStyleDefinition {
        private static final int ID_BEGIN_INDEX = 3;
        private static final int ID_END_INDEX = 4;

        private final ParagraphDefault paragraphDefault;
        private String id;

        ParagraphLevel(final ParagraphDefault paragraphDefault) {
            this.paragraphDefault = paragraphDefault;
        }

        @Override
        public String id() {
            return this.id;
        }

        @Override
        public void readWith(final XMLEventReader reader) throws XMLStreamException {
            this.id = readId();
            this.paragraphDefault.readWith(reader);
        }

        private String readId() {
            return this.paragraphDefault.startElement.getName().getLocalPart()
                .substring(ID_BEGIN_INDEX, ID_END_INDEX);
        }

        @Override
        public RunProperties runProperties() {
            return this.paragraphDefault.runProperties();
        }

        @Override
        public Markup toMarkup() {
            return this.paragraphDefault.toMarkup();
        }
    }
}
