/*
 * =============================================================================
 *   Copyright (C) 2010-2019 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.filters.openxml;

import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.StartElement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Provides strippable attributes.
 */
interface StrippableAttributes {

    /**
     * Strips start element attributes.
     *
     * @param startElementContext      A start element context
     * @param strippableAttributeNames Strippable attribute names
     *
     * @return A new start element with strippable attributes removed
     */
    default StartElement strip(StartElementContext startElementContext, Set<String> strippableAttributeNames) {
        List<Attribute> newAttributes = new ArrayList<>();

        Iterator currentAttributesIterator = startElementContext.getStartElement().getAttributes();

        while (currentAttributesIterator.hasNext()) {
            Attribute attribute = (Attribute) currentAttributesIterator.next();

            if (!strippableAttributeNames.contains(attribute.getName().getLocalPart())) {
                newAttributes.add(attribute);
            }
        }

        return startElementContext.getEventFactory().createStartElement(startElementContext.getStartElement().getName(),
                newAttributes.iterator(),
                startElementContext.getStartElement().getNamespaces());
    }

    /**
     * Provides a general strippable attributes.
     */
    final class General implements StrippableAttributes {
        /**
         * Attribute names to strip.
         */
        private final Set<String> names;

        General(final ConditionalParameters conditionalParameters) {
            final Stream<String> unconditional = Stream.of(
                    Name.General.SPELLING_ERROR.value(),
                    Name.General.NO_PROOFING.value(),
                    Name.General.DIRTY.value(),
                    Name.General.SMART_TAG_CLEAN.value(),
                    Name.General.LANG.value()
            );
            final Stream<String> merged;
            if (conditionalParameters.getCleanupAggressively()) {
                merged = Stream.concat(unconditional, Stream.of(Name.General.SPACING.value()));
            } else {
                merged = unconditional;
            }
            names = merged.collect(Collectors.toSet());
        }

        StartElement strip(final StartElementContext startElementContext) {
            return strip(startElementContext, names);
        }
    }

    final class ParagraphRevisions implements StrippableAttributes {

        private static final Set<String> PARAGRAPH_REVISION_ATTRIBUTES = Stream.of(
            Name.Revision.RPR.value(),
            Name.Revision.DEL.value(),
            Name.Revision.R.value(),
            Name.Revision.P.value(),
            Name.Revision.R_DEFAULT.value()
        ).collect(Collectors.toSet());

        StartElement strip(StartElementContext startElementContext) {
            return strip(startElementContext, PARAGRAPH_REVISION_ATTRIBUTES);
        }
    }

    final class RunRevisions implements StrippableAttributes {
        private static final Set<String> RUN_REVISION_ATTRIBUTES = Stream.of(
                Name.Revision.RPR.value(),
                Name.Revision.DEL.value(),
                Name.Revision.R.value()
        ).collect(Collectors.toSet());

        StartElement strip(StartElementContext startElementContext) {
            return strip(startElementContext, RUN_REVISION_ATTRIBUTES);
        }
    }

    final class TableRowRevisions implements StrippableAttributes {
        private static final Set<String> TABLE_ROW_REVISION_ATTRIBUTES = Stream.of(
            Name.Revision.RPR.value(),
            Name.Revision.DEL.value(),
            Name.Revision.R.value(),
            Name.Revision.TR.value()
        ).collect(Collectors.toSet());

        StartElement strip(StartElementContext startElementContext) {
            return strip(startElementContext, TABLE_ROW_REVISION_ATTRIBUTES);
        }
    }

    final class SectionPropertiesRevisions implements StrippableAttributes {
        private static final Set<String> SECTION_PROPERTIES_REVISION_ATTRIBUTES = Stream.of(
            Name.Revision.RPR.value(),
            Name.Revision.DEL.value(),
            Name.Revision.R.value(),
            Name.Revision.SECT.value()
        ).collect(Collectors.toSet());

        StartElement strip(StartElementContext startElementContext) {
            return strip(startElementContext, SECTION_PROPERTIES_REVISION_ATTRIBUTES);
        }
    }

    /**
     * Provides strippable attributes names.
     */
    interface Name {
        String value();

        /**
         * Provides a general names enumeration.
         */
        enum General implements Name {
            SPELLING_ERROR("err"),
            NO_PROOFING("noProof"),
            DIRTY("dirty"),
            SMART_TAG_CLEAN("smtClean"),
            LANG(XMLEventHelpers.LOCAL_PROPERTY_LANGUAGE),
            SPACING("spc");

            private final String value;

            General(String value) {
                this.value = value;
            }

            @Override
            public String value() {
                return value;
            }
        }

        /**
         * Provides a revision names enumeration.
         */
        enum Revision implements Name {
            RPR("rsidRPr"),
            DEL("rsidDel"),
            R("rsidR"),
            SECT("rsidSect"),
            P("rsidP"),
            R_DEFAULT("rsidRDefault"),
            TR("rsidTr");

            private final String value;

            Revision(String value) {
                this.value = value;
            }

            @Override
            public String value() {
                return value;
            }
        }
    }
}
